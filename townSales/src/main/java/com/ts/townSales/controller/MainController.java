package com.ts.townSales.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class MainController {

	@RequestMapping(value= {"", "/", "/error", "/*"})
	public String mainPage() {
		log.debug("main controller");
		return "index";
	}
	
	
	
}
