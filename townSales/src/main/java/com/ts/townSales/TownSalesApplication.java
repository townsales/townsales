package com.ts.townSales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TownSalesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TownSalesApplication.class, args);
		//commit
	}

}

